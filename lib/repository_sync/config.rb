require 'yaml'
module RepositorySync
  @repositories = {}

  class Config
    attr_accessor :repositories

    def initialize(config_file='./repositories.yaml')
      @repositories = YAML.load_file(config_file)['repositories']
    end

  end
end
