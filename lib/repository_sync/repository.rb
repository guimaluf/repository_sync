module RepositorySync
  class Repository
    attr_accessor :name, :origin, :mirrors

    def initialize(name:, origin:, mirrors: [])
      @name = name
      @origin = origin
      @mirrors = mirrors
    end

  end
end
