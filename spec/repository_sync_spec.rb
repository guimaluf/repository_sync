RSpec.describe RepositorySync do
  it "has a version number" do
    expect(RepositorySync::VERSION).not_to be nil
  end

  describe "webhook" do
    it "receive request"
    it "validates request"
    it "trigger origin mirroring"
  end
end
