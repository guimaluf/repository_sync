RSpec.describe RepositorySync::Repository do
  subject {
    RepositorySync::Repository.new(name: 'my_repo',
                                   origin: 'g@g.com/g/repository_sync',
                                   mirrors: [ 'g@g2.com/g/r_s' ])
  }

  it "returns attributes" do
    expect(subject.name).to eq('my_repo')
    expect(subject.origin).to eq('g@g.com/g/repository_sync')
    expect(subject.mirrors).to eq([ 'g@g2.com/g/r_s' ])
  end

  it "create origin repo"
  it "keep origin in sync"
  it "add mirrors to origin"
  it "push origin to mirrors"
end
