RSpec.describe RepositorySync::Config do

  subject { RepositorySync::Config.new('spec/fixtures/repositories.yaml') }

  it "load config file" do
    expect(subject.repositories).not_to be_empty
  end

  it "contain repository" do
    expect(subject.repositories.size).to be(2)
  end

  it "contain custom repositories" do
    expect(subject.repositories['my_repo']).to eq(
      {
        'origin'  => 'https://gitlab.com/guimaluf/repository_sync',
        'mirrors' => [ 'https://github.com/guimaluf/repository_sync' ],
      }
    )

    expect(subject.repositories['another_repo']).to eq(
      {
        'origin'  => 'https://gitlab.com/guimaluf/repository_sync',
        'mirrors' => [
          'https://github.com/guimaluf/repository_sync',
          'https://bitbucket.com/guimaluf/repository_sync',
        ],
      }
    )
  end
end
